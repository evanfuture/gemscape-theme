# [Gemscape Theme](http://evanpayne.com/gemscape)

Gemscape is a Wordpress theme for building a tourism website.

* Source: [https://github.com/evanfuture/townscape](https://github.com/evanfuture/gemscape)
* Documentation: Forthcoming

