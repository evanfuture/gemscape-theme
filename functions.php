<?php
/**
 * Gemscape includes
 *
 * The $gemscape_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 */
$gemscape_includes = [
  'lib/post-types.php',
  'lib/utils.php',           // Utility functions
  'lib/init.php',            // Initial theme setup and constants
  'lib/wrapper.php',         // Theme wrapper class
  'lib/config.php',          // Configuration
  'lib/assets.php',          // Scripts and stylesheets
  'lib/titles.php',          // Page titles
  'lib/nav.php',             // Custom nav modifications
  'lib/gallery.php',         // Custom [gallery] modifications
  'lib/extras.php',          // Custom functions
  'lib/breadcrumbs.php',          // Custom breadcrumbs
  'lib/acf-fields/acf-fields.php'
];

foreach ($gemscape_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'gemscape'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);
