	<?php $splash = get_field('gemscape_site_splash', 'option'); ?>
	<?php $splash_logo = get_field('gemscape_site_logo', 'option'); ?>
	<div id="splash" class="splash section" style="background-image:url('<?php echo $splash;?>');background-position:center top;">
		<div class="logo">
			<img src="<?php echo $splash_logo;?>" />
		    <h1>The Ring of <br/><span>Beara</span></h1>
	    </div>
	</div>
	<?php while (have_posts()) : the_post(); ?>
		<div class="introduction section">
			<?php the_content();?>
		</div>
	<?php endwhile; ?>

	<?php
		$taxonomies = array( 'area' );
		$args = array(
		    'orderby'           => 'id',
		    'order'             => 'ASC',
		    'hide_empty'        => false,
		    'parent'			=> 0
		);
	$terms = get_terms($taxonomies, $args);
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){?>
	    <div class="area_list section">
	     	<h2>Explore by Area</h2>
	    <?php foreach ( $terms as $term ) {?>
	    	<div class="area">
	    		<?php
	    		$url = get_term_link($term);
	    		$image = get_field('gemscape_area_main_image', $term);
	    		?>

				<a href="<?php echo $url; ?>" title="<?php echo $term->name; ?>">
					<?php
					if( !empty($image) ){
						$title = $image['title'];
						$alt = $image['alt'];
						$caption = $image['caption'];

						// thumbnail
						$size = 'gemscape_thin';
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ];
						?>
						<img class="area-image" src="<?php echo $thumb; ?>" alt="<?php echo $term->name; ?> on The Ring of Beara" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
					<?php } else{
			            $term_title = $term->name;
			            $stringtitle = str_replace(" ", "+", $term_title);?>
			            <img src="http://placehold.it/700x340&text=<?php echo $stringtitle;?>" class="area-image">
			       <?php }?>
		       	</a>
		       	<div class="area-info">
			    	<img class="small-map" src="<?php echo get_field('gemscape_area_mini_map', $term);?>" />
			       	<h3><?php echo $term->name;?></h3>
			       	<?php
					$subareas = get_term_children( $term->term_id, 'area' );
					if(!empty($subareas)){?>
						<div class="sub-areas">
						<?php foreach ( $subareas as $subarea ) {
							$term2 = get_term_by( 'id', $subarea, 'area' );
							echo '<a class="standard" href="' . get_term_link( $subarea, 'area' ) . '">' . $term2->name . '</a>';
						}?>
						</div>
					<?php }?>
			       	<p><?php echo get_field('gemscape_area_short_description', $term);?></p>
			       	<div class="arrow-left"></div>
			       	<a href="<?php echo $url; ?>" title="Click to Read More About <?php echo $term->name; ?>">View More</a>
		       	</div>
			</div>
	    <?php }?>
	    </div>
	<?php }?>

	<?php
		$taxonomies = array( 'gem_type' );
		$args = array(
		    'orderby'           => 'count',
		    'order'             => 'ASC',
		    'hide_empty'        => false,
		    'parent'			=> 0,
		    'exclude_tree'      => array(220,218,217), // routes, galleries, events
		);
	$terms = get_terms($taxonomies, $args);
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){?>
	    <div class="gem_type_list section">
	     	<h2>Explore by Category</h2>
	    <?php foreach ( $terms as $term ) {?>
	    	<div class="gem_type">
	    		<?php
	    		$url = get_term_link($term);
	    		$image = get_field('gemscape_gem_type_main_image', $term);
	    		?>
		       	<div class="gem_type_info">
			       	<h3><?php echo $term->name;?></h3>
			       	<?php
					$gem_type_children = get_term_children( $term->term_id, 'gem_type' );
					if(!empty($gem_type_children)){?>
						<div class="sub_gem_types">
							<a class="standard" href="<?php echo $url;?>">All</a>
						<?php foreach ( $gem_type_children as $gem_type ) {
							$term2 = get_term_by( 'id', $gem_type, 'gem_type' );
							echo '<a class="standard" href="' . get_term_link( $gem_type, 'gem_type' ) . '">' . $term2->name . '</a>';
						}?>
						</div>
					<?php }?>
			       	<a href="<?php echo $url; ?>" title="Explore in <?php echo $term->name; ?>">View More</a>
		       	</div>
			</div>
	    <?php }?>
	    </div>
	<?php }?>