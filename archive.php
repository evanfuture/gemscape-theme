	<?php
	global $wp_query;
	$term = $wp_query->get_queried_object();
	$term_id = $term->term_id;
	$term_link = get_term_link( $term, $taxonomy );
	?>
<div class="archive-header section" >
	<div class="archive-basics">
		<h1><a href="<?php echo $term_link;?>" alt="<?php echo $term->name;?>"><?php echo $term->name;?></a> on the Ring of Beara</h1>
		<p class="description"><?php echo $term->description;?></p>
	</div>
</div>
<div class="archive-content section">

	<?php if (is_tax('gem_type' )){
				$args = array(
					'post_type' => 'gem',
					'tax_query' => array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'gem_type',
							'field'    => 'slug',
							'terms'    => $term->slug
						),
					)
		        );
				$query2 = new WP_Query( $args );
				?>
				<main class="archive_gems section cd-main-content">
					<div class="cd-tab-filter-wrapper">
						<div class="cd-tab-filter">
							<ul class="cd-filters">
								<li class="placeholder">
									<a data-type="all" href="#0">Everything</a> <!-- selected option on mobile -->
								</li>
								<li class="filter"><a class="selected" href="#0" data-type="all">All</a></li>
								<?php
								$taxonomies = array( 'gem_type' );
									$args = array(
									    'orderby'           => 'ID',
									    'order'             => 'ASC',
									    'hide_empty'        => false,
									    'parent'			=> $term_id,
									);
								$terms = get_terms($taxonomies, $args);
								$gem_subtype_slugs = array();
								foreach ( $terms as $term ) {
									?>
							    		<li class="filter" data-filter=".<?php echo $term->slug;?>"><a href="#0" data-type="<?php echo $term->slug;?>"><?php echo $term->name;?></a></li>
						    		<?php
						    	}?>
							</ul> <!-- cd-filters -->
						</div> <!-- cd-tab-filter -->
					</div> <!-- cd-tab-filter-wrapper -->

					<section class="cd-gallery">
						<div class="legend">
							<span class="beara"></span>Ring of Beara <span class="kenmare"></span>Kenmare.com <a class="tooltip" href="#"><i class="icon-info"></i><span class="tooltip-content"><span class="tooltip-text"><span class="tooltip-inner">Boxes with a green background will <br/>open on our sister site, Kenmare.com.</span></span></span></a>
						</div>
						<ul>
						<?php if($query2->have_posts()) { while ( $query2->have_posts() ):$query2->the_post();
							$gem_types = get_the_terms( $post->ID, 'gem_type' );
						    if ( $gem_types && ! is_wp_error( $gem_types ) ) :
						    	$gem_type_classes = "";
						    	$gem_type_parent_names = "";
						      $gem_type_names = array();
							  $gem_type_parents = array();
						      foreach ( $gem_types as $gem_type ) {
						        $gem_type_names[] = $gem_type->slug;
						        $parent="";
						        $parent = get_term_by('id', $gem_type->parent, 'gem_type');
						        	if(!empty($parent)){
								        $gem_type_parents[] = $parent->slug;
								    }
						      }
						      $gem_type_classes = join( " ", $gem_type_names );
						      $gem_type_parent_names = join( " ", $gem_type_parents );
						    endif;?>
							<li class="mix <?php echo $gem_type_classes;?>  <?php echo $gem_type_parent_names;?>">
						    	<figure class="slide-link gem_small">
									<?php if ( has_post_thumbnail() ) {
							            the_post_thumbnail('gemscape_thumb');
							        }
							        else{
							            $title = get_the_title();
							            $stringtitle = str_replace(" ", "+", $title);
							            echo '<img src="http://placehold.it/200x130&text='.$stringtitle.'" />';
							        } ?>
									<figcaption>
										<h3><?php the_title();?></h3>
										<p><?php echo get_field('gemscape_subtitle');?></p>
										<div class="meta">
											<span class="gem-type icon-gem icon-<?php echo join(" icon-", $gem_type_parents);?>" title="<?php echo join("/", $gem_type_parents);?>"></span>
										</div>
										<a href="<?php the_permalink();?>" title="Read More about <?php the_title();?>">View More</a>
									</figcaption>
								</figure>
							</li>
					    <?php endwhile; }
					    wp_reset_postdata();?>
					    <?php
					    	global $wp_query;
							$term = $wp_query->get_queried_object();
							$data = file_get_contents('http://ringofbeara.com/wp-content/themes/gemscape/assets/kenmare.json');
							$kenmare_gems = json_decode( $data );
							$this_slug = get_term_link( $term, $taxonomy );
							$partial_slug = str_replace("http://ringofbeara.com/", "", $this_slug);

							foreach($kenmare_gems as $gem){
								$gem_types = $gem->terms->business_type;
								$gem_type_slugs = array();
								foreach($gem_types as $gem_type_object){
									$gem_type_tmp = $gem_type_object->slug;
									$slug = str_replace("-kenmare", "", $gem_type_tmp);
									$gem_type_slugs[] = $slug;

									if(isset($gem_type_object->parent->slug)){
									$gem_type_tmp = $gem_type_object->parent->slug;
									$slug = str_replace("-kenmare", "", $gem_type_tmp);
									$gem_type_slugs[] = $slug;
									}
								}
								if (in_array($term->slug, $gem_type_slugs)){
								?>
									<li class="mix kenmare <?php echo join( " ", $gem_type_slugs );?>">
								    	<figure class="slide-link gem_small">
											<?php
												$gem_image = "";
												if(isset($gem->featured_image->attachment_meta->sizes->townscape_thumb->url)){
													$gem_image = $gem->featured_image->attachment_meta->sizes->townscape_thumb->url;
												}
												if(!empty( $gem_image )){
													echo '<img src="'.$gem_image.'" />';
												}else {
									            $title = $gem->title;
									            $stringtitle = str_replace(" ", "+", $title);
									            echo '<img src="http://placehold.it/200x130&text='.$stringtitle.'" />';
									        }
									        ?>
											<figcaption>
												<h3><?php echo $gem->title;?></h3>
												<p>Open This on Kenmare.com</p>
												<div class="meta">
													<span class="gem-type icon-gem"></span>
												</div>
												<a href="<?php echo $gem->link;?>#<?php echo $partial_slug;?>" title="Read More about <?php echo $gem->title;?>">View More</a>
											</figcaption>
										</figure>
									</li>
								<?php
								}
							} ?>

							<li class="gap"></li>
							<li class="gap"></li>
							<li class="gap"></li>
						</ul>
					</section>
					</div><!--/listings-section-->
			<?php } ?>

</div>
