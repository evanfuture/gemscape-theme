<?php
namespace Gemscape;
use Gemscape\Config;
use Gemscape\Wrapper;
?>

<?php if ( is_page_template( 'template-html.php' ) || is_singular( 'sa_invoice' ) || is_singular('sa_estimate' )) {
  include Wrapper\template_path();
} else { ?>

<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

  <!--[if lt IE 9]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'gemscape'); ?>
    </div>
  <![endif]-->

  <?php
    do_action('get_header');
    get_template_part('templates/header');
    ?><div class="cd-main-content"><?php
    get_template_part('templates/breadcrumbs');

    include Wrapper\template_path();

    get_template_part('templates/footer');
  ?>

</body>
</html>

<?php }?>