<?php

namespace Gemscape\Init;

use Gemscape\Assets;

/**
 * Theme setup
 */
function setup() {
  // Make theme available for translation
  load_theme_textdomain('gemscape', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'gemscape')
  ]);

  // Add post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');
  add_image_size('gemscape_thumb',    200, 130, true);
  add_image_size('gemscape_thumb_thin', 320, 130, true);
  add_image_size('gemscape_full',     860, 9999);
  add_image_size('gemscape_listing',  350, 9999);
  add_image_size('gemscape_thin',  700, 340, true);
  add_image_size('gemscape_long',  1000, 340, true);
  add_image_size('gemscape_gallery',  875, 530, true);



  // Add HTML5 markup for captions
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption']);

  // Tell the TinyMCE editor to use a custom stylesheet
  add_editor_style(Assets\asset_path('styles/editor-style.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');
