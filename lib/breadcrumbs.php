<?php
namespace Gemscape\Extras;
/**
Custom Breadcrumbs
**/
function gemscape_breadcrumbs() {

  $delimiter = '&raquo;'; // delimiter between crumbs
  $home = '<span class="icon-home"></span>The Ring of Beara'; // text for the 'Home' link
  $before = '<span class="active">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb

  global $post;
  $homeLink = get_bloginfo('url');

  if (is_home() || is_front_page()) {

  } else {

    echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';

    if ( is_category() ) {
      $thisCat = get_category(get_query_var('cat'), false);
      if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
      echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;

    } elseif ( is_tax('gem_type') ) {
    $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $parent = get_term($term->parent, get_query_var('taxonomy') );
    if ($term->parent != 0) echo '<a href="/' . $parent->slug . '">' . $parent->name . '</a> ' . $delimiter . ' ';
    echo '<a href="/' . $term->slug . '" class="active">' . $term->name . '</a>';

    } elseif (is_tax('area')) {
    $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $parent = get_term($term->parent, get_query_var('taxonomy') );
    if ($term->parent != 0) echo '<a href="/areas/' . $parent->slug . '">' . $parent->name . '</a> ' . $delimiter . ' ';
    echo '<a href="/areas/' . $term->slug . '" class="active">' . $term->name . '</a>';


  } elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;

    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;

    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;

    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;

    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() == 'gem' ) {
        $post_type = get_post_type_object(get_post_type());
        $terms = get_the_terms( $post->ID, 'area' );
        if ( $terms && ! is_wp_error( $terms ) ) {
          $tax_links = array();
          foreach ( $terms as $term ) {
            $tax_links[] = $term->name;
            $out[] =
            '  <a href="'
          .    get_term_link( $term->slug, 'area' ) .'">'
          .    $term->name
          . "</a>\n";
          }
          $tax = join( " / ", $out );
          $slug = $post_type->rewrite;
          echo $tax;
        }
        echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category();
        $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        echo $cats;
        echo $before . get_the_title() . $after;
      }

    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;

    } elseif ( is_attachment() ) {
      $attachment_id = get_the_ID(); // attachment ID
      $attachment_meta = wp_get_attachment($attachment_id);
      echo $attachment_meta['title'] . '.jpg';

    } elseif ( is_page() && !$post->post_parent ) {
      echo $before . get_the_title() . $after;

    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
      }
      echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;

    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;

    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;

    } elseif ( is_404() ) {
      echo $before . '404 Error - Page Not Found' . $after;
    }

    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }

  }
} // end gemscape_breadcrumbs()
