<?php

/**
Gems
 */

function gem_cpt() {
  $labels = array(
    'name'                => _x( 'Gems', 'Post Type General Name', 'gemscape' ),
    'singular_name'       => _x( 'Gem', 'Post Type Singular Name', 'gemscape' ),
    'menu_name'           => __( 'Gems', 'gemscape' ),
    'parent_item_colon'   => __( 'Parent Gem:', 'gemscape' ),
    'all_items'           => __( 'All Gems', 'gemscape' ),
    'view_item'           => __( 'View Gem', 'gemscape' ),
    'add_new_item'        => __( 'Add New Gem', 'gemscape' ),
    'add_new'             => __( 'Add New', 'gemscape' ),
    'edit_item'           => __( 'Edit Gem', 'gemscape' ),
    'update_item'         => __( 'Update Gem', 'gemscape' ),
    'search_items'        => __( 'Search Gem', 'gemscape' ),
    'not_found'           => __( 'Not found', 'gemscape' ),
    'not_found_in_trash'  => __( 'Not found in Trash', 'gemscape' ),
  );
  $rewrite = array(
    'slug'                => 'gems',
    'with_front'          => true,
    'pages'               => true,
    'feeds'               => true,
  );
  $args = array(
    'label'               => __( 'Gem', 'gemscape' ),
    'description'         => __( 'Points of Interest', 'gemscape' ),
    'labels'              => $labels,
    'supports'            => array( 'title', 'editor', 'thumbnail', ),
    'taxonomies'          => array( 'area', 'gem_type' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-location',
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'rewrite'             => $rewrite,
    'capability_type'     => 'post',
  );
  register_post_type( 'gem', $args );

  $labels = array(
    'name'                       => _x( 'Areas', 'Taxonomy General Name', 'gemscape' ),
    'singular_name'              => _x( 'Area', 'Taxonomy Singular Name', 'gemscape' ),
    'menu_name'                  => __( 'Areas', 'gemscape' ),
    'all_items'                  => __( 'All Areas', 'gemscape' ),
    'parent_item'                => __( 'Parent Area', 'gemscape' ),
    'parent_item_colon'          => __( 'Parent Area:', 'gemscape' ),
    'new_item_name'              => __( 'New Area Name', 'gemscape' ),
    'add_new_item'               => __( 'Add New Area', 'gemscape' ),
    'edit_item'                  => __( 'Edit Area', 'gemscape' ),
    'update_item'                => __( 'Update Area', 'gemscape' ),
    'separate_items_with_commas' => __( 'Separate Areas with commas', 'gemscape' ),
    'search_items'               => __( 'Search Areas', 'gemscape' ),
    'add_or_remove_items'        => __( 'Add or remove Areas', 'gemscape' ),
    'choose_from_most_used'      => __( 'Choose from the most used Areas', 'gemscape' ),
    'not_found'                  => __( 'Not Found', 'gemscape' ),
  );
  $rewrite = array(
    'slug'                       => 'areas',
    'with_front'                 => true,
    'hierarchical'               => true,
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite'                    => $rewrite,
  );
  register_taxonomy( 'area', array( 'gem' ), $args );

  $labels = array(
    'name'                       => _x( 'Gem Types', 'Taxonomy General Name', 'gemscape' ),
    'singular_name'              => _x( 'Gem Type', 'Taxonomy Singular Name', 'gemscape' ),
    'menu_name'                  => __( 'Gem Types', 'gemscape' ),
    'all_items'                  => __( 'All Gem Types', 'gemscape' ),
    'parent_item'                => __( 'Parent Gem Type', 'gemscape' ),
    'parent_item_colon'          => __( 'Parent Gem Type:', 'gemscape' ),
    'new_item_name'              => __( 'New Gem Type Name', 'gemscape' ),
    'add_new_item'               => __( 'Add New Gem Type', 'gemscape' ),
    'edit_item'                  => __( 'Edit Gem Type', 'gemscape' ),
    'update_item'                => __( 'Update Gem Type', 'gemscape' ),
    'separate_items_with_commas' => __( 'Separate Gem Types with commas', 'gemscape' ),
    'search_items'               => __( 'Search Gem Types', 'gemscape' ),
    'add_or_remove_items'        => __( 'Add or remove Gem Types', 'gemscape' ),
    'choose_from_most_used'      => __( 'Choose from the most used Gem Types', 'gemscape' ),
    'not_found'                  => __( 'Not Found', 'gemscape' ),
  );
  $rewrite = array(
    'slug'                       => 'gem-type',
    'with_front'                 => true,
    'hierarchical'               => true,
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite'                    => $rewrite,
  );
  register_taxonomy( 'gem_type', array( 'gem' ), $args );

  $labels = array(
    'name'                       => _x( 'Schemata', 'Taxonomy General Name', 'gemscape' ),
    'singular_name'              => _x( 'Schema', 'Taxonomy Singular Name', 'gemscape' ),
    'menu_name'                  => __( 'Schemata', 'gemscape' ),
    'all_items'                  => __( 'All Schemata', 'gemscape' ),
    'parent_item'                => __( 'Parent Schema', 'gemscape' ),
    'parent_item_colon'          => __( 'Parent Schema:', 'gemscape' ),
    'new_item_name'              => __( 'New Schema Name', 'gemscape' ),
    'add_new_item'               => __( 'Add New Schema', 'gemscape' ),
    'edit_item'                  => __( 'Edit Schema', 'gemscape' ),
    'update_item'                => __( 'Update Schema', 'gemscape' ),
    'separate_items_with_commas' => __( 'Separate Schemata with commas', 'gemscape' ),
    'search_items'               => __( 'Search Schemata', 'gemscape' ),
    'add_or_remove_items'        => __( 'Add or remove Schemata', 'gemscape' ),
    'choose_from_most_used'      => __( 'Choose from the most used Schemata', 'gemscape' ),
    'not_found'                  => __( 'Not Found', 'gemscape' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => false,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => false,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'schema', array( 'gem' ), $args );

}
add_action( 'init', 'gem_cpt', 0 );

add_filter( 'manage_edit-gem_columns', 'gem_columns' ) ;
function gem_columns( $columns ) {

  $columns = array(
    'cb' => '<input type="checkbox" />',
    'post_thumbs' => __('Thumbs', 'gemscape'),
    'title' => __( 'Name', 'gemscape' ),
    'gem_type' => __( 'Gem Type', 'gemscape' ),
    'gemscape_gps_lng' => __( 'Longitude' ),
    'date' => __( 'Date' )
  );

  return $columns;
}

add_action( 'manage_gem_posts_custom_column', 'manage_gem_columns', 10, 2 );

function manage_gem_columns( $column, $post_id ) {
  global $post;
  global $wpdb;
  switch( $column ) {
    /* If displaying the 'duration' column. */
    case 'post_thumbs' :
      echo the_post_thumbnail( 'gemscape_thumb' );
      break;
    case 'gemscape_gps_lng' :
      /* Get the post meta. */
      $gemscape_gps_lng = get_post_meta( $post_id, 'gemscape_gps_lng', true );

      /* If no location is found, output a default message. */
      if ( empty( $gemscape_gps_lng ) ) {
        echo __( 'Unknown' );
      }

      else {
        echo $gemscape_gps_lng;
      }

      break;


    /* If displaying the 'gem type' column. */
    case 'gem_type' :

      /* Get the gem types for the post. */
      $terms = get_the_terms( $post_id, 'gem_type' );

      /* If terms were found. */
      if ( !empty( $terms ) ) {

        $out = array();

        /* Loop through each term, linking to the 'edit posts' page for the specific term. */
        foreach ( $terms as $term ) {
          $out[] = sprintf( '<a href="%s">%s</a>',
            esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'gem_type' => $term->slug ), 'edit.php' ) ),
            esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'gem_type', 'display' ) )
          );
        }

        /* Join the terms, separating them with a comma. */
        echo join( ', ', $out );
      }

      /* If no terms were found, output a default message. */
      else {
        _e( 'No Gem Types' );
      }

      break;

    /* Just break out of the switch statement for everything else. */
    default :
      break;
  }
}

add_filter( 'manage_edit-gem_sortable_columns', 'gem_sortable_columns' );

function gem_sortable_columns( $columns ) {

  $columns['gemscape_gps_lng'] = 'gemscape_gps_lng';

  return $columns;
}
/* Only run our customization on the 'edit.php' page in the admin. */
add_action( 'load-edit.php', 'gem_edit_load' );

function gem_edit_load() {
  add_filter( 'request', 'gem_sort' );
}

/* Sorts the movies. */
function gem_sort( $vars ) {

  /* Check if we're viewing the 'movie' post type. */
  if ( isset( $vars['post_type'] ) && 'gem' == $vars['post_type'] ) {

    /* Check if 'orderby' is set to 'location'. */
    if ( isset( $vars['orderby'] ) && 'gemscape_gps_lng' == $vars['orderby'] ) {

      /* Merge the query vars with our custom variables. */
      $vars = array_merge(
        $vars,
        array(
          'meta_key' => 'gemscape_gps_lng',
          'orderby' => 'meta_value'
        )
      );
    }
  }

  return $vars;
}


/**
Include Gems in Search Results
**/
add_filter( 'pre_get_posts', 'cpt_search' );
/**
 * This function modifies the main WordPress query to include an array of
 * post types instead of the default 'post' post type.
 *
 * @param object $query  The original query.
 * @return object $query The amended query.
 */
function cpt_search( $query ) {

    if ( $query->is_search ) {
  $query->set( 'post_type', array( 'post', 'gem' ) );
    }

    return $query;
  }