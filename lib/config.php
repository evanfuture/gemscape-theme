<?php

namespace Gemscape\Config;

use Gemscape;
/**
 * Enable theme features
 */
add_theme_support('jquery-cdn');            // Enable to load jQuery from the Google CDN
add_theme_support('soil-clean-up');
add_theme_support('soil-nice-search');
add_theme_support('soil-disable-trackbacks');
add_theme_support('soil-disable-asset-versioning');


/**
 * Configuration values
 */
define('GOOGLE_ANALYTICS_ID', 'UA-59884749-1'); // UA-XXXXX-Y (Note: Universal Analytics only, not Classic Analytics)

if (!defined('WP_ENV')) {
  define('WP_ENV', 'production');  // assets.php checks for values 'production' or 'development'
}

/**
 * $content_width is a global variable used by WordPress for max image upload sizes
 * and media embeds (in pixels).
 *
 * Example: If the content area is 640px wide, set $content_width = 620; so images and videos will not overflow.
 * Default: 1140px is the default Bootstrap container width.
 */
if (!isset($content_width)) { $content_width = 1140; }
