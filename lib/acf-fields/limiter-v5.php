<?php
class acf_field_limiter extends acf_field {
	function __construct() {
		$this->name = 'limiter';
		$this->label = __('Limiter', 'acf-limiter');
		$this->category = 'basic';
		$this->defaults = array(
			'character_number'	=> 150,
			'display_characters'	=> 1,
		);
		$this->l10n = array(
			'error'	=> __('Error! Please enter a higher value', 'acf-limiter'),
		);
    	parent::__construct();
	}

	function render_field_settings( $field ) {
		acf_render_field_setting( $field, array(
			'label'			=> __('Character number','acf-limiter'),
			'instructions'	=> __('How many characters would you like to limit','acf-limiter'),
			'type'			=> 'number',
			'name'			=> 'character_number',
			'append'		=> 'characters'
		));
		acf_render_field_setting( $field, array(
			'label' => 'Display character limit',
			'instructions'	=> __('Show the number of characters left over the top of the progress bar.','acf-limiter'),
			'type'  =>  'radio',
			'name'  =>  'display_characters',
			'choices' =>  array(
			1 =>  __("No",'acf'),
			0 =>  __("Yes",'acf'),
			),
			'layout'  =>  'horizontal'
		));
	}

	function render_field( $field ) {
		$field['value'] = str_replace('<br />','',$field['value']);
		echo '<textarea id="' . $field['id'] . '" class="limiterField" data-characterlimit="'.$field['character_number'].'" rows="4" class="' . $field['class'] . '" name="' . $field['name'] . '" >' . $field['value'] . '</textarea>';
		echo('<div id="progressbar-'.$field['id'].'" class="progressBar"></div>');
		if(isset($field['display_characters'])){
			if($field['display_characters'] == 0){
				echo('<div class="counterWrapper"><span class="limiterCount"></span> / <span class="limiterTotal">'.$field['character_number'].'</span></div>');
			}
		}
	}

	function input_admin_enqueue_scripts() {
		$dir = 'http://ringofbeara.com/wp-content/themes/gemscape/lib/acf-fields/';
		wp_register_script( 'acf-input-limiter', "{$dir}js/limiter.js" );
		wp_register_style( 'acf-input-limiter', "{$dir}css/limiter.css" );
		wp_register_style( 'jquery-ui-progressbar.min', "{$dir}css/jquery-ui-progressbar.min.css" );
		wp_enqueue_script(array(
			'acf-input-limiter','jquery-ui-progressbar'
		));
		wp_enqueue_style(array(
			'jquery-ui-progressbar.min','acf-input-limiter'
		));
	}
}

new acf_field_limiter();
?>