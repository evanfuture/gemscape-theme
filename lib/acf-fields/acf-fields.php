<?php
class acf_new_fields {

	function __construct()
	{
		add_action('acf/include_field_types', array($this, 'include_new_field_types') );
	}

	function include_new_field_types() {
		include_once('range-v5.php');
		include_once('limiter-v5.php');
		include_once('address-v5.php');
	}
}

new acf_new_fields();
?>