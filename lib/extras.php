<?php

namespace Gemscape\Extras;

/**
Include ACF Fields
**/
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title'  => 'Site Settings',
    'menu_title'  => 'Site Settings',
    'menu_slug'   => 'site-settings',
    'capability'  => 'edit_themes',
  ));
}

/**
Clean up the_excerpt()
**/
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'gemscape') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
Get All Attachment Metadata
  Custom code from: http://wordpress.stackexchange.com/questions/125554/get-image-description
**/
function wp_get_attachment( $attachment_id ) {

$attachment = get_post( $attachment_id );
return array(
    'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
    'caption' => $attachment->post_excerpt,
    'description' => $attachment->post_content,
    'href' => get_permalink( $attachment->ID ),
    'src' => $attachment->guid,
    'title' => $attachment->post_title
);
}

/**
Add Connections between Gems and Sprout Clients
**/

function my_connection_types() {

    p2p_register_connection_type( array(
        'name' => 'gem_to_client',
        'from' => 'sa_client',
        'to' => 'gem',
        'cardinality' => 'one-to-many'
    ) );
    p2p_register_connection_type( array(
        'name' => 'gem_to_invoice',
        'from' => 'sa_invoice',
        'to' => 'gem',
        'cardinality' => 'one-to-many'
    ) );
    p2p_register_connection_type( array(
        'name' => 'gem_to_surl',
        'from' => 'surl',
        'to' => 'gem',
        'cardinality' => 'one-to-one'
    ) );

}
add_action( 'p2p_init',  __NAMESPACE__ . '\\my_connection_types' );

/**
Hide Menu Items for non-admins
**/
function remove_menu_items() {
    if( !current_user_can( 'administrator' ) ):
      remove_menu_page( 'edit.php?post_type=gem' );
      remove_menu_page( 'edit.php?post_type=sa_invoice' );
      remove_menu_page( 'edit.php?post_type=sa_estimate' );
      remove_menu_page('tools.php');
      remove_menu_page('acf-options');
    endif;
}
add_action( 'admin_menu',  __NAMESPACE__ . '\\remove_menu_items' );


/**
Display Gems by Name
**/

function gemscape_alpha_display( $query ) {

    if ( is_archive() ) {
        $query->set( 'orderby', 'name' );
        $query->set( 'order', 'ASC' );
        return;
    }
}
add_action( 'pre_get_posts',  __NAMESPACE__ . '\\gemscape_alpha_display', 1 );

/**
Circles on Static Maps
**/
function mapboxCircle($Lat,$Lng,$Rad,$Detail=8){
  $R    = 6371;

  $pi   = pi();

  $Lat  = ($Lat * $pi) / 180;
  $Lng  = ($Lng * $pi) / 180;
  $d    = $Rad / $R;

  $points = array();
  $i = 0;

  for($i = 0; $i <= 360; $i+=$Detail):
    $brng = $i * $pi / 180;

    $pLat = asin(sin($Lat)*cos($d) + cos($Lat)*sin($d)*cos($brng));
    $pLng = (($Lng + atan2(sin($brng)*sin($d)*cos($Lat), cos($d)-sin($Lat)*sin($pLat))) * 180) / $pi;
    $pLat = ($pLat * 180) /$pi;

    $points[] = array($pLat,$pLng);
  endfor;

  /* create encoded polyline from the points */
  require_once('maps/PolylineEncoder.php');
  $PolyEnc   = new PolylineEncoder($points);
  $EncString = $PolyEnc->dpEncode();

  return $EncString['Points'];
}


/**
JSON from Kenmare.com
**/

// Add a custom controller
add_filter('json_api_controllers', 'add_my_controller');
function add_my_controller($controllers) {
  $controllers[] = 'Gemscape';
  return $controllers;
}

// Register the source file for our controller
add_filter('json_api_gemscape_controller_path', 'gemscape_controller_path');
function gemscape_controller_path($default_path) {
  return get_stylesheet_directory() . '/lib/json.php';
}


function slug_api_posts_url_string( $post_types = 'post', $filters = false, $rooturl = 'home_url', $base = 'wp-json/posts?' ) {
  if ( is_callable( $rooturl ) ) {
    $url = call_user_func( $rooturl );
    $url = $url.$base;
  }
  else {
    $url = $rooturl.$base;
  }
  if ( is_string(  $post_types ) ) {
    $post_types = array( $post_types);
  }
  foreach ( $post_types as $type ) {
    $url = add_query_arg( "type[]", $type, $url  );
  }
  if ( $filters ) {
    foreach( $filters as $filter => $value ) {
      $args[ "filter[{$filter}]" ] =  $value;
    }
    $url = add_query_arg( $args, $url );
  }
  return $url;
}

function slug_get_json( $url ) {
  //GET the remote site
  $response = wp_remote_get( $url );
  //Check for error
  if ( is_wp_error( $response ) ) {
    return sprintf( 'The URL %1s could not be retrieved.', $url );
  }
  //get just the body
  $data = wp_remote_retrieve_body( $response );
  //return if not an error
  if ( ! is_wp_error( $data )  ) {
    //decode and return
    return json_decode( $data );
  }
}