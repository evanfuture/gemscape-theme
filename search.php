<div class="search section">

	<h1 class="search-title"><?php echo $wp_query->found_posts; ?> <?php _e( 'Search Results Found For', 'gemscape' ); ?>: "<?php the_search_query(); ?>"</h1>

  <?php while (have_posts()) : the_post(); ?>
    <?php
		$card_class = get_post_type();
		$card_classes = array(
			'card',
			'mini',
			$card_class
			);
		?>
		<a alt="View More about <?php the_title();?>" href="<?php the_permalink();?>">
	        <article <?php post_class($card_classes);?>>
	              <?php
		        $post_id = get_the_id();
		        $user_id = get_current_user_id();
		        $bookmarks = (array) get_user_meta($user_id, '_wpb_bookmarks', true);
	            $bookmarked_class = '';
		        if (isset($bookmarks[$post_id])){
		            $bookmarked_class = 'bookmarked';
		        }
		        ?>
				<div class="page-content <?php echo $bookmarked_class?>">
			        <?php if ( has_post_thumbnail() ) {
			            the_post_thumbnail('gemscape_thumb', array('class' => 'listing-main-image'));
			        }
			        else{
			            $title = get_the_title();
			            $stringtitle = str_replace(" ", "+", $title);
			            echo '<img src="http://placehold.it/300x195&text='.$stringtitle.'" class="listing-main-image">';
			        } ?>

					<?php
						$location_lat = get_field('gemscape_coordinates');
						if( !empty($location_lat) ){?>
							<img src="http://kenmare.com/wp-content/themes/gemscape/assets/img/map-marker.png" style="position: relative; top: 19px; float: right;" width="16" height="16" class="has-map">
					<?php }?>
		            <h2 class="listing-name title">
		                <?php the_title();?>
		            </h2>
		            <?php
		                $subtitle = get_field('gemscape_subtitle');
		                if( !empty($subtitle)) {?>
		                      <h3 class="subtitle"><?php echo $subtitle;?></h3>
		                <?php }
		            ?>
				</div>
	            <div class="more-link primary-button">View More</div>
	        </article>
	    </a>
  <?php endwhile; ?>

  <?php get_search_form( true ); ?>

</div>