<?php
/*
Template Name: HTML Template
*/
?>

<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

    <style type="text/css">
        html {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
            background:none;
        }
        body {
            max-width: inherit;
            padding-top:0;
            background-color:<?php echo get_field('theme_body_bg_color');?>
        }
            <?php echo get_field('theme_custom_css');?>
        }
    </style>

    <?php echo get_field ('theme_html_content'); ?>
    <?php wp_footer(); ?>

</body>
</html>