<div class="search-container">
	<div id="sb-search" class="sb-search">
		<form role="search" method="get" id="searchform" class="searchform search-form form-inline" action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<input class="sb-search-input" placeholder="<?php _e('Search for:', 'gemscape'); ?>" type="search" value="<?php echo get_search_query(); ?>" name="s" id="s">
			<input class="sb-search-submit" type="submit" value="">
			<span class="sb-icon-search icon-search"></span>
		</form>
	</div>
</div>