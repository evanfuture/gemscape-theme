<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php wp_title('|', true, 'right'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

<?php if (is_singular('gem')) {?>

  <?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'gemscape_gallery' );?>

  <meta name="description" content="<?php echo get_field('gemscape_short_description');?>" />

  <!-- Schema.org markup for Google+ -->
  <meta itemprop="name" content="<?php the_title_attribute();?>">
  <meta itemprop="description" content="<?php echo get_field('gemscape_short_description');?>">
  <meta itemprop="image" content="<?php echo $image_url[0];?>">

  <!-- Twitter Card data -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@ringofbeara">
  <meta name="twitter:title" content="<?php the_title_attribute();?>">
  <meta name="twitter:description" content="<?php echo get_field('gemscape_short_description');?>">
  <meta name="twitter:creator" content="@ringofbeara">
  <!-- Twitter summary card with large image must be at least 280x150px -->
  <meta name="twitter:image:src" content="<?php echo $image_url[0];?>">

  <!-- Open Graph data -->
  <meta property="og:title" content="<?php the_title_attribute();?>" />
  <meta property="og:type" content="place" />
  <meta property="og:url" content="<?php the_permalink();?>" />
  <meta property="og:image" content="<?php echo $image_url[0];?>" />
  <meta property="og:description" content="<?php echo get_field('gemscape_short_description');?>" />
  <meta property="og:site_name" content="RingofBeara.com" />
  <?php $location = get_field('gemscape_coordinates');?>
  <meta property="place:location:latitude" content="<?php echo $location['lat'];?>" />
  <meta property="place:location:longitude" content="<?php echo $location['lng'];?>" />

<?php } ?>

  <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo esc_url(get_feed_link()); ?>">

  <link rel="apple-touch-icon" sizes="57x57" href="/wp-content/themes/gemscape/dist/images/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/wp-content/themes/gemscape/dist/images/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/wp-content/themes/gemscape/dist/images/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/wp-content/themes/gemscape/dist/images/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/wp-content/themes/gemscape/dist/images/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/wp-content/themes/gemscape/dist/images/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/wp-content/themes/gemscape/dist/images/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/wp-content/themes/gemscape/dist/images/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/gemscape/dist/images/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="/wp-content/themes/gemscape/dist/images/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="/wp-content/themes/gemscape/dist/images/favicon-194x194.png" sizes="194x194">
  <link rel="icon" type="image/png" href="/wp-content/themes/gemscape/dist/images/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="/wp-content/themes/gemscape/dist/images/android-chrome-192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="/wp-content/themes/gemscape/dist/images/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="/wp-content/themes/gemscape/dist/images/manifest.json">
  <link rel="shortcut icon" href="/wp-content/themes/gemscape/dist/images/favicon.ico">
  <meta name="msapplication-TileColor" content="#2b5797">
  <meta name="msapplication-TileImage" content="/wp-content/themes/gemscape/dist/images/mstile-144x144.png">
  <meta name="msapplication-config" content="/wp-content/themes/gemscape/dist/images/browserconfig.xml">
  <meta name="theme-color" content="#c6c4b2">

  <?php wp_head(); ?>

</head>