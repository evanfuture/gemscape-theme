
</div><!-- /.cd-main-content -->
<footer class="site-footer" role="contentinfo">
	<div class="social-footer">
		<a href="https://www.facebook.com/ExploreBeara"><span class="icon-facebook social"></span></a>
		<a href="https://twitter.com/ringofbeara"><span class="icon-twitter social"></span></a>
		<a href="mailto:info@ringofbeara.com"><span class="icon-email social"></span></a>
	</div>
	<div class="credits">
		<p><a href="http://ringofbeara.com"><?php echo get_bloginfo('name' );?></a> is designed and maintained by <a href="http://kenmarecreative.com">Kenmare Creative</a>.&nbsp;&nbsp;All Rights Reserved.</p>
    </div>
</footer>

	<nav id="cd-lateral-nav">
		<ul class="cd-navigation">
		<p>Explore</p>

		<?php
		$taxonomies = array( 'area' );
		$args = array(
		    'orderby'           => 'id',
		    'order'             => 'ASC',
		    'hide_empty'        => false,
		    'parent'			=> 0
		);
		$terms = get_terms($taxonomies, $args);
		if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){?>
			<li class="item-has-children">
				<a href="#0">Areas</a>
				<ul class="sub-menu">
					<?php foreach ( $terms as $term ) {
						$url = get_term_link($term);
						?>
						<li><a href="<?php echo $url;?>"><?php echo $term->name; ?></a></li>
					<?php }?>
				</ul>
			</li> <!-- item-has-children -->
		<?php }?>
		<?php
		$taxonomies = array( 'gem_type' );
		$args = array(
		    'orderby'           => 'count',
		    'order'             => 'ASC',
		    'hide_empty'        => false,
		    'parent'			=> 0,
		    'exclude_tree'      => array(220,218,217), // routes, galleries, events
		);
		$terms = get_terms($taxonomies, $args);
		if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){?>
			<li class="item-has-children">
				<a href="#0">Categories</a>
				<ul class="sub-menu">
					<?php foreach ( $terms as $term ) {
						$url = get_term_link($term);
						?>
						<li><a href="<?php echo $url;?>"><?php echo $term->name; ?></a></li>
					<?php }?>
				</ul>
			</li> <!-- item-has-children -->
		<?php }?>

		</ul> <!-- cd-navigation -->

		<div class="cd-navigation socials">
			<a class="cd-twitter cd-img-replace" href="http://twitter.com/ringofbeara/"></a>
			<a class="cd-facebook cd-img-replace" href="https://www.facebook.com/ExploreBeara"></a>
		</div> <!-- socials -->
	</nav>

<?php wp_footer(); ?>
		<script src="/wp-content/themes/gemscape/assets/scripts/classie.js"></script>
		<script src="/wp-content/themes/gemscape/assets/scripts/uisearch.js"></script>
		<script>
			new UISearch( document.getElementById( 'sb-search' ) );
		</script>

	<?php if (is_tax()){?>
	<script src="/wp-content/themes/gemscape/assets/scripts/jquery.mixitup.min.js"></script>
	<script src="/wp-content/themes/gemscape/assets/scripts/filter.js"></script>
	<?php } ?>

	<?php if (is_single()){?>
	<script src="/wp-content/themes/gemscape/assets/scripts/cd-slider.js"></script>
	<?php } ?>
