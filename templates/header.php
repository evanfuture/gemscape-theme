<header class="site-header" role="banner">
    <?php get_search_form(); ?>

    <?php $splash_logo = get_field('gemscape_site_logo', 'option'); ?>
    <div class="logo-sm">
      <img src="<?php echo $splash_logo;?>" />
        <h1><a href="<?php echo get_home_url();?>" alt="Home"/>The Ring of <span>Beara</span></a></h1>
      </div>

  <div class="menu">
    <a id="cd-menu-trigger" href="#0"><span class="cd-menu-text">Menu</span><span class="cd-menu-icon"></span></a>
  </div>
  <!-- The nav menu itself is actually in the footer.php -->

</header>
