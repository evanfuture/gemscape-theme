<?php
/*
Table of Contents:
- PHP Variables
- Scripts
- Card Shell
  - Schema
  - Bookmark/Share
  - Ownership/Sponsored By
  - Gem Type
- Featured Image
- Title
- Subtitle
- Short Description
- Website Button
- Phone
- Email
- Website (yes, twice)
- Location
- Address
- Get Directions
- Specifics by Gem Type
  - Accommodation
  - Dining
  - Activities
  - Shopping
  - Routes
  - Services
  - Galleries
  - Events
- Long Description
*/

  /**
  - PHP Variables
  **/

  $post_id = get_the_id();
  $user_id = get_current_user_id();
  $gem_title = get_the_title();
  $stringtitle = str_replace(" ", "+", $gem_title);

  /**
  - Card Shell
  **/

  /* Schema */
  $schemata = get_the_terms( $post->ID, 'schema' );
    if ( $schemata && ! is_wp_error( $schemata ) ) {
      $schema_names = array();
      foreach ( $schemata as $schema ) {
        $schema_links[] = 'http://schema.org/' . $schema->name;
        $schema_slugs[] = $schema->slug;
      }
      $schema = join( " ", $schema_links );
      $schema_class = join(" ", $schema_slugs );
    }

  /* Bookmark/Share */
  $gem_bookmark_class = '';
  $gem_bookmark = '<a class="save-this" href="#" alt="Save This"><span class="icon-save"></span></a>';
  $bookmarks = (array) get_user_meta($user_id, '_wpb_bookmarks', true);
    if (isset($bookmarks[$post_id])){
        $gem_bookmark_class = 'bookmarked';
        $gem_bookmark = '<a class="save-this saved" href="#" alt="UnSave This"><span class="icon-save"></span></a>';
    }

  /* Ownership/Sponsored By */
  $gem_ownership_class = '';
  $gem_ownership = '';
  $ownership = get_field('gemscape_ownership');
  $gem_ownership_class = $ownership;
    if ($ownership == 'public'){
      $sponsor = get_field('gemscape_sponsored_by');
        if (!empty($sponsor)){
          $post = $sponsor;
          setup_postdata( $post );
          $gem_ownership = 'This Public Listing is sponsored by: <a href="'.get_the_permalink().'">'.get_the_title().'</a>';
          wp_reset_postdata();
        } else {
          $gem_ownership = '<a href="mailto:info@kenmarecreative.com?subject=Sponsorship of the '.get_the_title().' listing on RingofBeara.com" alt="Sponsor this Listing">This Public Listing can be sponsored.</a>';
        }
    }

  /* Gem Type */
  $gem_types = get_the_terms( $post->ID, 'gem_type' );
  $gem_type_classes = '';
    if ( $gem_types && ! is_wp_error( $gem_types ) ) :
      $gem_type_names = array();
      foreach ( $gem_types as $gem_type ) {
        $gem_type_names[] = $gem_type->slug;
      }
      $gem_type_classes = join( " ", $gem_type_names );
    endif;

  /* The Container */

  echo '<div itemscope itemtype="'.$schema.'" class="'.$schema_class.' '.$gem_bookmark_class.' '.$gem_ownership_class.' '.$gem_type_classes.'">';

?>


<section class="cd-single-item basic_info">
  <div class="cd-slider-wrapper basic_info_image">
<?php
/**
  - Featured Image
  **/

  $gemscape_gallery = '';
  $gallery_images = get_field('gemscape_gallery');
    if(!empty($gallery_images)){
      echo '<ul class="cd-slider"><li class="selected">';
        the_post_thumbnail('gemscape_gallery', array('class' => 'listing-main-image'));
      echo '</li>';
      foreach( $gallery_images as $image ){
        $gemscape_gallery .= '<li><img src="'.$image['sizes']['gemscape_gallery'].'" alt="'.$image['alt'].'" /></li>';
      }
      echo $gemscape_gallery;
      echo '</ul>';
      echo '<ul class="cd-slider-navigation"><li><a href="#0" class="cd-prev inactive">Next</a></li><li><a href="#0" class="cd-next">Prev</a></li></ul><a href="#0" class="cd-close">Close</a>';
    } else {
      if ( has_post_thumbnail() ) {
        echo '<ul class="cd-slider"><li class="selected">';
        the_post_thumbnail('gemscape_gallery', array('class' => 'listing-main-image'));
        echo '</li></ul><a href="#0" class="cd-close">Close</a>';
      } else{
        echo '<img src="http://placehold.it/875x530&text='.$stringtitle.'" class="listing-main-image">';
      }
    }
?>
  </div>
  <div class="cd-item-info">
  <div class="basic_info_text">
<div class="info_icons"><span class="icon-gem"></span><span class="icon-save"></span></div>

<?
  /**
  - Title
  **/

  echo '<h1 itemprop="name" class="gem_title title">'.$gem_title.'</h1>';

  /**
  - Subtitle
  **/

  $gem_subtitle = '';
  $subtitle = get_field('gemscape_subtitle');
    if( !empty($subtitle)) {
      $gem_subtitle = '<div class="gem_subtitle">'.$subtitle.'</div>';
    }

  echo $gem_subtitle;
  ?>

  <?php

  /**
  - Website Button
  **/
?>

  <script>
    var trackOutboundLink = function(url) {
      ga('send', 'event', 'outbound', 'click', url, {'hitCallback': function () { document.location = url; } });
    };
  </script>


 <?php
  $gem_website_button = '';
  $gem_url = get_field('gemscape_url');
  $go_url = new WP_Query( array(
      'connected_type' => 'gem_to_surl',
      'connected_items' => get_queried_object(),
    ) );
    if ( $go_url->have_posts() ) {
      while ( $go_url->have_posts() ) : $go_url->the_post();
        $gem_url = get_permalink();
        $gem_website_button = '<a alt="Visit '.$gem_title.'" href="'.$gem_url.'" onclick="trackOutboundLink('.$gem_url.'); return false;" class="gem_website_link primary-button btn" itemprop="url" target="_blank">Visit Website</a>';
      endwhile; wp_reset_postdata();
    }

  echo $gem_website_button;


  /**
  - Short Description
  **/
  $gem_description = '';
  $description = get_field('gemscape_short_description');
    if( !empty($description)) {
      $gem_description = '<div class="gem_description" itemprop="description">'.$description.'</div>';
    }
  echo $gem_description;


?>

</div>
  </div>
</section> <!-- cd-single-item -->

<div class="split contact_info">
<div>
  <?php
  /**
  - Location
  **/

  $location = get_field('gemscape_coordinates');
    if( !empty($location) ){
      $location_type = get_field('gemscape_location');
        if ($location_type == 'point'){?>
          <div class="static-map">
            <img src="http://api.tiles.mapbox.com/v4/kenmarecreative.dc788e6a/pin-s+d00(<?php echo $location['lng']; ?>,<?php echo $location['lat']; ?>)/<?php echo $location['lng']; ?>,<?php echo $location['lat']; ?>,14/450x240.png?access_token=pk.eyJ1Ijoia2VubWFyZWNyZWF0aXZlIiwiYSI6IkhJUlB4MTAifQ.72y0YoPhg0ZS33tVStGxZQ" />
          </div>
          <?php
          if(!empty($location)) {
            echo '<div class="directions"><a href="https://www.google.com/maps/dir/Current+Location/'.$location['lat'].','.$location['lng'].'" alt="Get Directions">Get Directions</a></div>';
          }
          ?>
          <div class="geo" itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
            <span class="lati">Latitude: <?php echo $location['lat'];?></span>
            <span class="longi">Longitude: <?php echo $location['lng'];?></span>
            <meta itemprop="latitude" content="<?php echo $location['lat'];?>" />
            <meta itemprop="longitude" content="<?php echo $location['lng'];?>" />
          </div>
        <?php } else {
          $area_disc = Gemscape\Extras\mapboxCircle($location['lat'],$location['lng'],30,4);?>
          <div class="static-map">
            <img src="http://api.tiles.mapbox.com/v4/kenmarecreative.dc788e6a/path+fff-10(<?php echo $area_disc;?>)/<?php echo $location['lng']; ?>,<?php echo $location['lat']; ?>,8/350x260.png?access_token=pk.eyJ1Ijoia2VubWFyZWNyZWF0aXZlIiwiYSI6IkhJUlB4MTAifQ.72y0YoPhg0ZS33tVStGxZQ" />
          </div>
          <div class="geo">
            <?php the_field('gemscape_service_area');?>
          </div>
        <?php }
    }
?>
</div>
<div>
 <h3>Contact Details</h3>


<?php
  /**
  - Phone
  **/

  $gem_phone = '';
  $phone = get_field('gemscape_phone');
    if (!empty($phone)) {
      $phone_cc = get_field('gemscape_phone_cc');
      $phone_prefix = get_field('gemscape_phone_prefix');
      $gem_phone = '<div class="phone" itemprop="telephone"><a class="gem_phone" href="tel:+'.$phone_cc.''.$phone_prefix.''.$phone.'"><span class="icon-phone"></span>(0'.$phone_prefix.') '.$phone.'</a></div>';
    }


  /**
  - Email
  **/

  $gem_email = '';
  $email = get_field('gemscape_email');
    if (!empty($email)) {
      $gem_email = '<div class="email" itemprop="email"><a class="gem_email" href="mailto:'.$email.'?subject=Found you on RingofBeara.com"><span class="icon-email"></span>'.$email.'</a></div>';
    }

  /**
  - Website (yes, twice)
  **/

  $gem_website_url = '';
  $website_url = get_field('gemscape_url');
    if (!empty($website_url)) {
      $gem_website_url = '<div class="website"><a class="website_url sameAs" itemprop="sameAs" href="'.$website_url.'">'.$website_url.'</a></div>';
    }


?>
<table cellspacing="0"> <!-- cellspacing='0' is important, must stay -->
  <tbody>
  <?php if(!empty($gem_phone)){?>
    <tr>
      <td>Phone:</td>
      <td><?php  echo $gem_phone;?></td>
    </tr>
  <?php } ?>
  <?php if(!empty($gem_email)){?>
    <tr class="even">
      <td>Email:</td>
      <td><?php  echo $gem_email;?></td>
    </tr>
    <?php } ?>
    <?php if(!empty($gem_website_url)){?>
    <tr>
      <td>Website:</td>
      <td><?php  echo $gem_website_url;?></td>
    </tr>
    <?php } ?>
    <?php
      /**
      - Address
      **/
    if( have_rows('gemscape_address_container') ){?>
    <tr class="even">
    <td>Address:</td>
    <?php
      while ( have_rows('gemscape_address_container') ) : the_row();
        if( get_row_layout() == 'gemscape_address' ){
          $address_1 = get_sub_field('gemscape_address_1');
          $address_2 = get_sub_field('gemscape_address_2');
          $city = get_sub_field('gemscape_city');
          $county = get_sub_field('gemscape_county');
          $country = get_sub_field('gemscape_country');
            if (!empty($county)) { ?>
            <td>
              <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="address">
                  <p class="listing-address">
                      <?php echo $gem_title;?><br/>
                      <?php if (!empty($address_1)) { ?>
                        <span itemprop="streetAddress"><?php echo $address_1;?></span><br/>
                      <?php }?>
                      <?php if(!empty($address_2)){?>
                        <span itemprop="streetAddress"><?php echo $address_2;?></span><br/>
                      <?php }?>
                      <?php if(!empty($city)){?>
                        <span itemprop="addressLocality"><?php echo $city;?></span>,&nbsp;
                      <?php }?>
                      <span itemprop="addressRegion">Co. <?php echo $county;?></span><br/>
                      <span itemprop="addressCountry"><?php echo $country;?></span>
                  </p>
              </div>
            </td>
            <?php }
           }
      endwhile;?>
      </tr>
    <?php } ?>

  </tbody>
</table>
</div>
</div>


<?php
  /**
  - Specifics by Gem Type
  **/



            $age_range = get_field_object('gemscape_age_range');
              $age_value = get_field('gemscape_age_range');
              $age_label = $age_range['choices'][ $age_value ];

          $duration = get_field('gemscape_duration');
          $environment = get_field('gemscape_environment');
          $activity_type = get_field('gemscape_activity_type');
          $price_range = get_field('gemscape_price_range');


  /* Accommodation */



  /* Dining */

  /* Activities */

  /* Shopping */

  /* Routes */

  /* Services */

  /* Galleries */

  /* Events */
?>

<?php
  /**
  - Long Description
  **/
  $long_description = get_field('gemscape_long_description');
    if( !empty($long_description) ){
      echo '<div class="gem_long_description">'.$long_description.'</div>';
    }
?>

</div>
