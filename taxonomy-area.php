	<?php
	global $wp_query;
	$term = $wp_query->get_queried_object();
	$splash = get_field('gemscape_area_splash', $term);
    if ( !empty($splash)) {
		$size = 'gemscape_long';
		$thumb = $splash['sizes'][ $size ];
    } else{
        $thumb = 'http://placehold.it/1000x340&text='.$term->name;
    }
	?>
<div class="area-header section" style="background-image:url('<?php echo $thumb;?>');background-position:center top;" >
	<div class="area-basics">
		<img class="small-map" src="<?php echo get_field('gemscape_area_mini_map', $term);?>" />
		<h1><?php echo $term->name;?></h1>
		<p class="meta-info"><span>pop.</span> - 1,231 c. 2014</p>
		<p class="meta-info"><span>area</span> - 3,128 km sq.</p>
		<p class="description"><?php echo $term->description;?></p>
	</div>
</div>
<div class="area-content section">
		<?php
		$subareas = get_term_children( $term->term_id, 'area' );
		if(!empty($subareas)){?>
			<div class="area_main_towns section">
				<h2>Main Areas</h2>
				<?php
				foreach ( $subareas as $subarea ) {
					$url = get_term_link( $subarea, 'area' );
					$term2 = get_term_by( 'id', $subarea, 'area' );
					$image = get_field('gemscape_area_main_image', $term2);
					?>
					<figure class="small-town slide-link">
						<?php
						if( !empty($image) ){
							$title = $image['title'];
							$alt = $image['alt'];
							$caption = $image['caption'];

							// thumbnail
							$size = 'gemscape_thumb_thin';
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ];
							?>
							<img class="small-town-image" src="<?php echo $thumb; ?>" alt="<?php echo $term2->name; ?> on The Ring of Beara" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
						<?php } else{
				            $term_title = $term2->name;
				            $stringtitle = str_replace(" ", "+", $term_title);?>
				            <img class="small-town-image" src="http://placehold.it/320x130&text=<?php echo $stringtitle;?>" alt="<?php echo $term2->name; ?> on The Ring of Beara" width="320" height="130" />
				       <?php }?>
						<figcaption>
							<h3><?php echo $term2->name;?></h3>
							<p><span class="icon-info">Learn More</span></p>
							<?php $mini_map = get_field('gemscape_area_mini_map', $term);
							if(!empty($mini_map))
								{}
								else{$mini_map = get_field('gemscape_area_mini_map', $term);
								}?>
							<img class="small-map" src="<?php echo $mini_map;?>" />
							<a href="<?php echo $url;?>" title="Read More about <?php echo $term2->name;?>">View More</a>
						</figcaption>
					</figure>
				<?php } ?>
			</div>
		<?php }?>

			<?php
				$args = array(
					'post_type' => 'gem',
					'tax_query' => array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'gem_type',
							'field'    => 'id',
							'terms'    => array( 220,218,217 ),
							'operator' => 'NOT IN',
						),
						array(
							'taxonomy' => 'area',
							'field'    => 'slug',
							'terms'    => $term->slug,
						),
					)
		        );
				$query2 = new WP_Query( $args );
				if($query2->have_posts()||$term->slug == 'kenmare-town' || $term->slug == 'northeast') {?>
				<main class="area_gems section cd-main-content">
					<h2>What's Here</h2>
					<div class="cd-tab-filter-wrapper">
						<div class="cd-tab-filter">
							<ul class="cd-filters">
								<li class="placeholder">
									<a data-type="all" href="#0">Everything</a> <!-- selected option on mobile -->
								</li>
								<li class="filter"><a class="selected" href="#0" data-type="all">All</a></li>
								<?php
								$taxonomies = array( 'gem_type' );
									$args = array(
									    'orderby'           => 'ID',
									    'order'             => 'ASC',
									    'hide_empty'        => false,
									    'parent'			=> 0,
									    'exclude_tree'      => array(220,218,217), // routes, galleries, events
									);
								$terms = get_terms($taxonomies, $args);
								$gem_subtype_slugs = array();
								foreach ( $terms as $term ) {
									?>
							    		<li class="filter" data-filter=".<?php echo $term->slug;?>"><a href="#0" data-type="<?php echo $term->slug;?>"><?php echo $term->name;?></a></li>
						    		<?php
						    	}?>
							</ul> <!-- cd-filters -->
						</div> <!-- cd-tab-filter -->
					</div> <!-- cd-tab-filter-wrapper -->

					<?php
					foreach($terms as $term) {
						?>
							<div class="cd-subtab-filter-wrapper <?php echo $term->slug;?>">
								<div class="cd-tab-filter">
									<ul class="cd-filters cd-subfilters">
										<li class="placeholder">
											<a data-type="all" href="#0">All <?php echo $term->name;?></a> <!-- selected option on mobile -->
										</li>
										<li class="filter" data-filter=".<?php echo $term->slug;?>"><a href="#0" data-type="<?php echo $term->slug;?>">All <?php echo $term->name;?></a></li>
										<?php
											$gem_type_children = get_term_children( $term->term_id, 'gem_type' );
											if(!empty($gem_type_children)){?>
												<?php foreach ( $gem_type_children as $gem_type ) {
													$term2 = get_term_by( 'id', $gem_type, 'gem_type' );
													?>
													<li class="filter subfilter" data-filter=".<?php echo $term2->slug;?>"><a href="#0" data-type="<?php echo $term2->slug?>"><?php echo $term2->name;?></a></li>
												<?php }
										} ?>
									</ul>
								</div> <!-- cd-tab-filter -->
							</div> <!-- cd-tab-filter-wrapper -->
						<?php
					}
					?>

					<section class="cd-gallery">
						<div class="legend">
							<span class="beara"></span>Ring of Beara <span class="kenmare"></span>Kenmare.com <a class="tooltip" href="#"><i class="icon-info"></i><span class="tooltip-content"><span class="tooltip-text"><span class="tooltip-inner">Boxes with a green background will <br/>open on our sister site, Kenmare.com.</span></span></span></a>
						</div>
						<ul>
						<?php while ( $query2->have_posts() ):$query2->the_post();
							$gem_types = get_the_terms( $post->ID, 'gem_type' );
						    if ( $gem_types && ! is_wp_error( $gem_types ) ) :
						    	$gem_type_classes = "";
						    	$gem_type_parent_names = "";
						      $gem_type_names = array();
							  $gem_type_parents = array();
						      foreach ( $gem_types as $gem_type ) {
						        $gem_type_names[] = $gem_type->slug;
						        $parent="";
						        $parent = get_term_by('id', $gem_type->parent, 'gem_type');
						        	if(!empty($parent)){
								        $gem_type_parents[] = $parent->slug;
								    }
						      }
						      $gem_type_classes = join( " ", $gem_type_names );
						      $gem_type_parent_names = join( " ", $gem_type_parents );
						    endif;?>
							<li class="mix <?php echo $gem_type_classes;?>  <?php echo $gem_type_parent_names;?>">
						    	<figure class="slide-link gem_small">
									<?php if ( has_post_thumbnail() ) {
							            the_post_thumbnail('gemscape_thumb');
							        }
							        else{
							            $title = get_the_title();
							            $stringtitle = str_replace(" ", "+", $title);
							            echo '<img src="http://placehold.it/200x130&text='.$stringtitle.'" />';
							        } ?>
									<figcaption>
										<h3><?php the_title();?></h3>
										<p><?php echo get_field('gemscape_subtitle');?></p>
										<div class="meta">
											<span class="gem-type icon-gem icon-<?php echo join(" icon-", $gem_type_parents);?>" title="<?php echo join("/", $gem_type_parents);?>"></span>
										</div>
										<a href="<?php the_permalink();?>" title="Read More about <?php the_title();?>">View More</a>
									</figcaption>
								</figure>
							</li>
					    <?php endwhile;?>

					    <?php global $wp_query;
							$term = $wp_query->get_queried_object();
						if($term->slug == 'kenmare-town' || $term->slug == 'northeast'){
							$data = file_get_contents('http://ringofbeara.com/wp-content/themes/gemscape/assets/kenmare.json');
							$kenmare_gems = json_decode( $data );
							$this_slug = get_term_link( $term, $taxonomy );
							$partial_slug = str_replace("http://ringofbeara.com/", "", $this_slug);

							foreach($kenmare_gems as $gem){
								$gem_types = $gem->terms->business_type;
								$gem_type_slugs = array();
								foreach($gem_types as $gem_type_object){
									$gem_type_tmp = $gem_type_object->slug;
									$slug = str_replace("-kenmare", "", $gem_type_tmp);
									$gem_type_slugs[] = $slug;

									if(isset($gem_type_object->parent->slug)){
									$gem_type_tmp = $gem_type_object->parent->slug;
									$slug = str_replace("-kenmare", "", $gem_type_tmp);
									$gem_type_slugs[] = $slug;
									}
								}
								?>
								<li class="mix kenmare <?php echo join( " ", $gem_type_slugs );?>">
							    	<figure class="slide-link gem_small">
										<?php
											$gem_image = "";
											if(isset($gem->featured_image->attachment_meta->sizes->townscape_thumb->url)){
												$gem_image = $gem->featured_image->attachment_meta->sizes->townscape_thumb->url;
											}
											if(!empty( $gem_image )){
												echo '<img src="'.$gem_image.'" />';
											}else {
								            $title = $gem->title;
								            $stringtitle = str_replace(" ", "+", $title);
								            echo '<img src="http://placehold.it/200x130&text='.$stringtitle.'" />';
								        }
								        ?>
										<figcaption>
											<h3><?php echo $gem->title;?></h3>
											<p>Open This on Kenmare.com</p>
											<div class="meta">
												<span class="gem-type icon-gem"></span>
											</div>
											<a href="<?php echo $gem->link;?>#<?php echo $partial_slug;?>" title="Read More about <?php echo $gem->title;?>">View More</a>
										</figcaption>
									</figure>
								</li>
							<?php }

						}?>

							<li class="gap"></li>
							<li class="gap"></li>
							<li class="gap"></li>
						</ul>
					</section>
					</div><!--/listings-section-->
				<?php } else {
					//no posts
				?>
				<div class="no-posts">
					<p>Sorry, but this section hasn't been filled yet.  If you know something that should be added, you can request it by sending an email to <a href="mailto:info@ringofbeara.com&subject=RingofBeara.com Suggestion&body=I think you should add ...">info@ringofbeara.com</a></p>
				</div>
				<?php }

				wp_reset_postdata();

		?>

</div>
